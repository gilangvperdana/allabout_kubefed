# Riset Kubefed
```
Kubernetes Federation Multi Cloud cluster.
```

### What's Inside

- [ ] Testing Ingress 
- [ ] Menambahkan Private Cluster kube / Public cluster kube

### Source
```
This is repository forked from Muhammad Ammar GitLab's [https://go.btech.id/ammar/riset-kubefeed]
```

### Reference
```
https://github.com/kubernetes-sigs/kubefed
```

